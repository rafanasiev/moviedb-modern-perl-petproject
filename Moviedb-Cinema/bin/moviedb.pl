#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  moviedb.pl
#
#        USAGE:  ./moviedb.pl
#
#  DESCRIPTION:  moviedb - a script to operate on MovieDB
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Ruslan A.Afanasiev (), <ruslan.afanasiev@gmail.com>
#      COMPANY:  Internet Research Systems LLC
#      VERSION:  1.0
#      CREATED:  01/25/2016 12:50:11 PM EET
#     REVISION:  ---
#===============================================================================
use lib '../lib';

use Modern::Perl;
use Getopt::Long qw(GetOptions);
use Pod::Usage qw(pod2usage);
use Moviedb::Controller;
use Moviedb::Menu;

pod2usage( -noperldoc => 1, -verbose => 2, -exitval => 0 ) unless @ARGV;
## CMD arguments for import, add etc
my @add_args;
my ($import_file, $loglvl) = (undef, 'INFO');

# get Moviedb::Controller object
my $ctrl = Moviedb::Controller->new;
# *** Process command line arguments ***
GetOptions(
    'import|i=s' => \$import_file,
    # NOTE: this is a trick to process multiple options for --add
    'add|a=s{4}'     => \@add_args, 
    'delete|d=i'     => sub { $ctrl->delete_movie($_[1]) },
    'show|s=i'       => sub { my $m = $ctrl->get_movie_by_('id', $_[1]); say $m },
    'by-title|t'     => sub { show_movies($ctrl, 'title') },
    'by-year|y'      => sub { show_movies($ctrl, 'year') },
    'find-by-name=s' => sub { get_movies($ctrl, 'title', $_[1]) },
    'find-by-star=s' => sub { get_movies($ctrl, 'stars', $_[1]) },
    'menu|m'         => sub { Moviedb::Menu::run($ctrl) },
    'loglevel=s'     => \$loglvl,
    'help|?|h' =>
      sub { pod2usage( -verbose => 2, -noperldoc => 1, -exitval => 0 ) },
);

## set loglevel
$ctrl->logger->adapter->{logger}->level($loglvl);


## NOTE: case for add
if (scalar(@add_args)) {
    $ctrl->add_new_movie(\@add_args);
}

## NOTE: case for import
if ($import_file) {
    $ctrl->import_from_txt(
        { source_file => $import_file }
    );
}

exit(0);

#
## HELPER SUBROUTINES
#

sub show_movies {
    my $controll = shift;
    my $attr = shift;
    my @movies = $controll->show_movies_by_($attr);
    printf "*** Found %d movies by %s ***\n", scalar @movies, $attr;
    exit(1) unless @movies;
    # pretty print - find the longest title
    my $max_len = 0;
    map {
        $max_len = length($_->title) if (length($_->title) > $max_len); 
    } @movies;

    for (@movies) {
        my $tmp_len = $max_len - length($_->title) + 1;
        printf("%s %${tmp_len}d\n", $_->title, $_->release_year); 
    }
    return;
}

sub get_movies {
    my $controll = shift;
    my $attr = shift;
    my $val  = shift;
    my @movies = @{ $controll->get_movies_by_($attr, $val) };
    printf "*** Found %d movies by %s ***\n", scalar @movies, $attr;
    exit(1) unless @movies;
    say $_ for (@movies);
    return;
}

__END__

=head1 NAME

  moviedb - a simple CLI program to manage your favorite movies DB

=head1 SYNOPSYS

  moviedb [-i <file.txt>], [-d #], [-a ARGS], [-s #], [-t], [-y], [--find-by-title title], [--find-by-star star], -m, [--loglevel TYPE]

    Options:
    -i, --import        import movies from a data source txt file. 
    -a, --add           add a movie's record. The argument must to be: 
                        $0 --add release_year=YYYY title='The movie' format=DVD stars='John Doe,Jack Moo' 
                        NOTE: the argument string should be quoted!

    -d, --delete        delete a movie by ID
    -s, --show          show properties of a movie by ID
    -t  --by-title      show list title and IDs of all movies, alphabetically 
    -y, --by-year       show list title, years and IDs of all movies, chronologically
        --find-by-name  find movies by subname 
        --find-by-star  find movies by star's name
    -m, --menu          script will prompt an interactive menu to manage database
        --loglevel      this option defines a log level - INFO, DEBUG etc

        --help  display this help and exit 

=cut
