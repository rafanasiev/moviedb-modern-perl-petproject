package Moviedb::Controller;
use Modern::Perl;
use Moo;
use MooX::Aliases;
use Carp qw(confess);
use Log::Any;
use Log::Any::Adapter;
use Log::Log4perl;
use YAML qw(Dump Load LoadFile);
use Data::Dumper;
use Moviedb::Cinema;

our $VERSION = '0.01';

has logger => ( 
    is      => 'ro',
    default => sub { Log::Any->get_logger },
);
has log_config => (
    is      => 'ro',
    default => qq(
        log4perl.rootLogger              = INFO, LOG1
        log4perl.appender.LOG1           = Log::Log4perl::Appender::File
        log4perl.appender.LOG1.filename  = moviedb.log
        log4perl.appender.LOG1.layout    = Log::Log4perl::Layout::PatternLayout
        log4perl.appender.LOG1.layout.ConversionPattern = %d - [%p] - %M:%L -- %m%n
    ), 
);
has source_file => (
    is => 'rw'
);
has infh => (
    is      => 'ro',
    default => sub { IO::File->new() },
);
has outfh => (
    is      => 'ro',
    default => sub { IO::File->new() },
);
has dbtype => (
    is => 'ro',
    default => 'yaml'
);
has database => ( 
    is      => 'rw',
    default => 'moviedb.yaml',
);

sub BUILDARGS {
    my $self = shift;
    my @args = @_;

    return {@args};
}

sub BUILD {
    my ($self, $args) = @_;
   
    ## NOTE: cannot use other database drivers except of YAML
    #say " --   ARGS are: " . Dumper($args);
    #say " -- DB type is: " . $args->{dbtype};
    if ( defined $args->{dbtype} && $args->{dbtype} ne 'yaml' ) {
        confess "Valid datatypes are: [yaml, json, sqlite]"
            unless ( grep { $_[0] eq $_ } qw(yaml json sqlite) );
    }
    # initialize logger to STDOUT
    Log::Log4perl::init( \$self->log_config );
    Log::Any::Adapter->set('Log::Log4perl');
    return;    
}

sub import_from_txt {
    my $self        = shift;
    my $import_args = shift;
    my $counter = 0; # calculates number of records
    # add source file attribute
    $self->source_file($import_args->{source_file});

    $self->logger->info("*** Import started ***");
    $self->logger->info("  Input  file name: " . $self->source_file);
    $self->logger->info("Output format file: " . $self->dbtype);
    $self->logger->info("  Output file name: " . $self->database);
    # open input file (reading) and output database (writing)
    $self->infh->open($self->source_file, 'r') or confess "Cannot open input file: $!";
    $self->outfh->open($self->database, 'w')   or confess "Cannot open database file: $!";


    # read input file and parse the content
    while ( my $line = readline($self->infh) ) {
        next if $line =~ m/^$/xms;
        chomp($line);

        # start process a movie record
        if ( $line =~ m/^Title|^Release\sYear|Format|Stars/xms ) {
            my @args = split_record($line);
            # read the rest of attributes, usually there are 3 records
            for (1 .. 3) {
                $line = readline($self->infh);
                chomp($line);
                push @args, split_record($line);
            }
            $self->logger->debug(" -- Input agrs");
            my %tmp = @args;
            $self->logger->debug("ini agr: $_ -> $tmp{$_}") for keys %tmp;
            # get a new object
            my $o = Moviedb::Cinema->new(\%tmp);
            $self->logger->debug($o);
            # serialize the object, the method <write> was inherited from IO::Handle
            $self->outfh->write(Dump($o) . "\n");
        }
        # count record's number
        ++$counter;
    }

    $self->logger->info("*** Import finished. Records: $counter ***");
    return $counter; 
}

around 'get_movies_by_' => sub {
    my $origin = shift;
    my $self   = shift;
    my ($attr, $value) = @_;

    no warnings 'experimental::smartmatch'; # disables warns for -> given, when, ~~ 
    for ($attr) {
        when (/id/) {
            $self->logger->info("*** Getting movie by ID ***");
            $self->logger->info("Id: $value");
            my $r = $origin->($self, $attr, $value);
            if (exists $r->[0]) {
                #say $r->[0];
                return $r->[0];
            }
            else {
                say "*** No records found ***";
            }
        }
        when (/stars/) {
            $self->logger->info("*** Getting movie by Star Name ***");
            $self->logger->info("Star name: $value");
            return $origin->($self, 'stars', $value);
        }
        when (/year/) {
            $self->logger->info("*** Getting movie by Release Year ***");
            $self->logger->info("Release year: $value");
            return $origin->($self, 'release_year', $value);
        }
        when (/title/) {
            $self->logger->info("*** Getting movie by Title ***");
            $self->logger->info("Title substring: $value");
            return $origin->($self, 'title', $value);
        }
        default {
            $self->logger->error("!! Unknow attribute <$attr>");
            return;
        }
    }
};

sub get_movies_by_ {
    my $self     = shift;
    my $obj_attr = shift;
    my $attr_val = shift;
    my @movies;

    # init generator
    my $rec = $self->iterate_over_txt_db(); 

    while ( my $o = $rec->() ) {
        ## NOTE: it works for Id and Release Year
        if ( $attr_val =~ m/^\d+$/xms ) {
            push @movies, $o if ( $o->$obj_attr == $attr_val );
        }
        else {
            ## NOTE: Stars are stored as list
            if ( $obj_attr eq lc 'stars' ) {
                $self->logger->debug(" -- Stars list: " . Dumper($o->$obj_attr));
                # "mark" a star to show it and save an object 
                for (my $i = 0; $i < scalar(@{$o->$obj_attr}); ++$i) {
                    if ( lc $o->$obj_attr->[$i] =~ m/$attr_val/ixms ) {
                        $o->$obj_attr->[$i] =~ s/^(.+)$/>> $1 <</xms;
                        push @movies, $o; 
                    }
                }
            }
            else {
                push @movies, $o if lc $o->$obj_attr =~ m/$attr_val/ixms;
            }
        }
    }
    $self->logger->warn("!! No records found !!") unless @movies;
    return \@movies;
}

alias get_movie_by_ => 'get_movies_by_';

sub show_movies_by_ {
    my $self     = shift;
    my $obj_attr = shift;
    my @movies;

    # init generator
    my $rec = $self->iterate_over_txt_db(); 

    while ( my $o = $rec->() ) {
        push @movies, $o;
    }

    no warnings 'experimental::smartmatch'; # disables warns for -> given, when, ~~ 
    for ($obj_attr) {
        when ('title') {
            @movies = sort { $a->title cmp $b->title } @movies;
            return @movies;
        }
        when ('year') {
            @movies = sort { $a->release_year <=> $b->release_year } @movies;
            return @movies;
        }
        default {
            @movies = sort { $a->id <=> $b->id } @movies;
            return @movies;
        }
    }
    return;
}

sub add_new_movie {
    my $self = shift;
    my $args = shift;
    $self->logger->info("*** Start to add a new record ***");
    # split args by = to get key, value sequence
    my %args = map { split('=', $_) } @$args;
    
    $self->logger->debug("--add ARGS: " . Dumper(\%args));
    # get last Id
    my $rec    = $self->iterate_over_txt_db();
    my $max_id = 0;
    while ( my $m = $rec->() ) {
        $max_id = $m->id if ($m->id > $max_id);
    }
    # create a new Moviedb::Cinema object
    $args{id} = ++$max_id;
    $self->logger->debug("Max. Id = $max_id");
    my $new_rec = Moviedb::Cinema->new(\%args);
    $self->logger->info("-- a new movie record:\n$new_rec");
    # append a new data into the database
    $self->outfh->open($self->database, 'a')   or confess "Cannot add a record into database file: $!";
    $self->outfh->write(Dump($new_rec) . "\n") or confess "Cannot write into database file: $!";
    $self->logger->info("*** New record Id=$max_id has been added ***");
    return;
}

sub delete_movie {
    my $self = shift;
    my $id   = shift;
    $self->logger->info("*** Deleting a record by Id=$id ***");
    my $rec = $self->iterate_over_txt_db();

    my (@movies, $seen_id);
    while ( my $m = $rec->() ) {
        push @movies, $m if $m->id != $id;
        $seen_id = $id   if $m->id == $id;
    }

    unless ($seen_id) {
        $self->logger->info("Id $id was not found.");
        return;
    }

    $self->logger->info("!! Database is going to be overwritten !!");
    $self->outfh->open($self->database, 'w') or confess "Cannot open database file: $!";

    foreach my $o (@movies) {
        $self->outfh->write(Dump($o) . "\n");
    }
    $self->logger->info("*** The record has been deleted ***");
    return $id;
}

sub iterate_over_txt_db {
    my $self = shift;
    my ($data_line, $obj);

    # read stored data from YAML db 
    $self->outfh->open($self->database, 'r') or confess "Cannot open database file: $!";

    # NOTE: a lazy generator that iterates over the db's records
    return sub {
        $data_line = readline($self->outfh);
        # read a single serialized record, it locates between --- AND \n
        if ( defined $data_line && $data_line =~ m/^---\s!!perl.+/xms ) {
            my $data = $data_line;
            while ( $data_line = readline($self->outfh) ) {
                last if $data_line =~ m/^$/xms; # it means here is the end of a single record
                $data .= $data_line; 
            }
            $self->logger->debug(" >> STRINGIFIED DATA: " . Dumper($data));
            # transform data from YAML into obj. notation
            $obj = Load($data);
            $self->logger->debug(" >> DESERIALIZED DATA: " . Dumper($obj));
            return $obj;
        }
    };
}

sub split_record {
    my $r = shift;
    my ($k, $v) = split(/:\s/x, $r, 2); # NOTE: we need key and value, a pare
    
    $k =~ s/\s/_/gxms;
    # trimming leading and ending spaces
    $v =~ s/(.+)?\s$/$1/xms;
    return lc $k, $v;
}

1; # End of Moviedb::Controller

__END__

=head1 NAME

Moviedb::Controller - does import from a source txt file

=head1 VERSION

Version 0.01


=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Moviedb::Controller;

    my $ctr = Moviedb::Controller->new;
    $ctr->import_from_txt('input.txt');
    ...

=head1 ATTRIBUTES

=over 4

=item * logger

=item * log_config

=item * infh

=item * outfh

=item * dbtype

=item * database

=back

=head1 BUILD AND COERCING METHODS

=head2 BUILD



=head1 SUBROUTINES/METHODS

=head2 import_from_txt

    Method arguments: a hash structure. The code could look like this:

        $ctrl->import_from_txt(
            {
                source_file => $db->filename, 
                dbtype => $dbtype
            }
        ); 

=over 4

=item * source_file => path to a file

=item * dbtype => a type of database, could be JSON, YAML etc. The default is YAML

=back

=head2 show_movies_by_

=head2 get_movie_by_id

=head2 add_new_movie

=head2 delete_movie

=head2 get_movies_by_

=head2 iterate_over_txt_db

=head2 split_record

=head1 AUTHOR

Ruslan Afanasiev, C<< <ruslan.afanasiev at gmail.com> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-moviedb-cinema at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=Moviedb-Cinema>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Moviedb::Cinema


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Moviedb-Cinema>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Moviedb-Cinema>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/Moviedb-Cinema>

=item * Search CPAN

L<http://search.cpan.org/dist/Moviedb-Cinema/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2016 Ruslan Afanasiev.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut


