package Moviedb::Menu;
use Modern::Perl;
use IO::Prompter;
use Moviedb::Controller;
use Log::Log4perl;
 
sub run {
    # moviedb controller
    my $mdbctrl = shift;
    # disables logging when we run interactive session
    Log::Log4perl->eradicate_appender("SCREEN");

    # infinity loop to show the menu
    while (1) {
        # menu composer
        my $user_input = prompt "*** MovieDB Interactive Terminal ***",
            -number,
            -menu => [ 
                'quit', 
                'add a record', 
                'show a record',
                'find movies by stars',
                'find movies by title',
                'find movies by year',
                'del a record'
            ],
            -style => 'bold white',
            -wipefirst,
            '>>';
        # exit 
        if ( $user_input eq 'quit' ) {
            say(" Exiting...");
            exit(0);
        }
        $mdbctrl->logger->debug("MENU: >> user input: $user_input");
        
        # process the input
        no warnings 'experimental::smartmatch'; # disables warns for -> given, when, ~~ 
        given ($user_input) {
            when (/add a record/)  {
                my @obj_args;
                my @attrs = qw(title release_year format stars);
                # collect object attributes
                foreach my $attr (@attrs) {
                    my $input = prompt " - Input movies $attr",
                        -style => 'bold red',
                        ' >>>';
                    push @obj_args, ($attr, $input);
                }
                # create a new record
                $mdbctrl->add_new_movie(\@obj_args);
                
            }
            when (/show a record/) {
                my $id = prompt " - Input record ID",
                    -number,
                    -style => 'bold blue',
                    ' >>>';
                $mdbctrl->logger->debug("MENU: >> record ID: $id");
                say $mdbctrl->get_movie_by_('id', $id);
            }
            when (/find movies by (stars|title|year)/) {
                my ($attr, $header) = ($1, undef);
                # compose header
                given ($attr) {
                    when (/stars/) { 
                        $header = ' - Input a star name or surname';
                    }
                    when (/title/) {
                        $header = ' - Input a movie title';
                    }
                    when (/year/) {
                        $header = ' - Input a relese year';
                        $attr   = 'release_year';

                    }
                    default { ... }
                }
                my $search_txt = prompt $header,
                    -style => 'bold green',
                    ' >>>';
                my @movies = @{ $mdbctrl->get_movies_by_($attr, $search_txt) };
                say "*** Found " . scalar(@movies) . " movies by $attr ***";
                exit(1) unless @movies;
                say $_ for (@movies);
            }
            when (/del a record/) { ... }
            default               { say "Unknown option. Exiting..."; exit(0); }
        }
    }
}

1;

__END__

=head1 NAME

Moviedb::Menu - provides an interactive menu to work with MovieDB 

=head1 VERSION

Version 0.01


=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Moviedb::Menu;

    Moviedb::Menu::run(); # <- shows the menu

=cut
