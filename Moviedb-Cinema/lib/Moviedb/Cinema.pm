package Moviedb::Cinema;

use Carp qw(confess);
use Data::Dumper;
use Moo;
use Types::Standard qw(Int Str ArrayRef Split);
use Readonly;

use overload '""' => '_stringify';

our $VERSION = '0.01';

# NOTE: add here your valid formats
my Readonly @VALID_FORMATS = qw(VHS DVD Blu-Ray);
my Readonly %VALID_FORMAT_MAP = map { $_ => 1 } @VALID_FORMATS;

has id => ( is => 'rw' );
has title => (
    is       => 'ro',
    isa      => Str,
    required => 1,
);
has release_year => (
    is       => 'ro',
    isa      => Int,
    required => 1,
);
has stars => (
    is       => 'ro',
    isa      => ( ArrayRef [Str] )->plus_coercions( Split [qr/,/] ),
    coerce   => 1,
    required => 1,
);
has format => (
    is  => 'ro',
    isa => sub {

        if ( !$VALID_FORMAT_MAP{ $_[0] } ) {
            confess "<$_[0]> does not supported! Valid are: @VALID_FORMATS";
        }
    },
    required => 1,
);

sub _list2string {
    my $self = shift;
    return join ', ', @{ $self->stars };
}

sub BUILDARGS {
    my ( $class, $args ) = @_;
    ## verify input arguments
    confess q{Invalid input string for 'stars'!}
      if $args->{stars} =~ m/[^a-z,\-\s]/ixms;
    confess q{String cannot be empty or <undef> for 'title'!}
      if $args->{title} =~ m/^$/xms;
    ## modify input arguments if we need them to modify
    $args->{stars} =~ s/,\s/,/gxms;

    return $args;
}

sub BUILD {
    my ( $self, $args ) = @_;

    ## get object's id if this is the first attempt, e.g. import of the data
    $self->id( Moviedb::GeneratorId->new->get_id ) unless defined( $self->id );

    return;
}

sub _stringify {
    my $self   = shift;
    my $format = <<'EOD';
       Id = %d
    Title = '%s'
Rel. year = %d
   Format = %s
    Stars = [%s]
EOD
    return sprintf $format,
        $self->id,     $self->title, $self->release_year,
        $self->format, $self->_list2string;
}

package Moviedb::GeneratorId;
use Modern::Perl;
use Moo;
use Data::Dumper;

{
    # NOTE: here is a trick to keep track of unique Id's
    state $COUNTER = 0;

    sub get_id {
        my $self = shift;
        return $COUNTER++;
    }
}

1;    # End of Moviedb::Cinema

__END__

=head1 NAME

Moviedb::Cinema - define a movie's object.

=head1 VERSION

Version 0.01


=head1 SYNOPSIS

Quick summary of what the module does.

Perhaps a little code snippet.

    use Moviedb::Cinema;

    my $foo = Moviedb::Cinema->new({
        title => 'The Muppet Movie',
        release_year => 1979,
        format => 'DVD',
        stars => 'Jim Henson, Frank Oz, Dave Geolz, Mel Brooks, James Coburn, Charles Durning'
    });
    ...

=head1 ATTRIBUTES

=over 4

=item * id

=item * title

=item * release_year

=item * stars

=item * format

=back

=head1 BUILD AND COERCING METHODS

=head2 BUILDARGS

=head2 BUILD

=head2 _list2string

=head2 _stringify


=head1 SUBROUTINES/METHODS


=head1 AUTHOR

Ruslan Afanasiev, C<< <ruslan.afanasiev at gmail.com> >>

=head1 BUGS

Please report any bugs or feature requests to C<bug-moviedb-cinema at rt.cpan.org>, or through
the web interface at L<http://rt.cpan.org/NoAuth/ReportBug.html?Queue=Moviedb-Cinema>.  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.




=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc Moviedb::Cinema


You can also look for information at:

=over 4

=item * RT: CPAN's request tracker (report bugs here)

L<http://rt.cpan.org/NoAuth/Bugs.html?Dist=Moviedb-Cinema>

=item * AnnoCPAN: Annotated CPAN documentation

L<http://annocpan.org/dist/Moviedb-Cinema>

=item * CPAN Ratings

L<http://cpanratings.perl.org/d/Moviedb-Cinema>

=item * Search CPAN

L<http://search.cpan.org/dist/Moviedb-Cinema/>

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

Copyright 2016 Ruslan Afanasiev.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


=cut


