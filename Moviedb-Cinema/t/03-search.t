use strict;
use warnings;
use 5.010;

use Test::More "no_plan";
use Test::Trap;
use File::Temp qw/ tempfile /;
use Moviedb::Controller;
#
# txt input data mimic
my $txt_data =<<EOL;
Title: Blazing Saddles
Release Year: 1974
Format: VHS
Stars: Mel Brooks, Clevon Little, Harvey Korman, Gene Wilder, Slim Pickens, Madeline Kahn

Title: Casablanca
Release Year: 1942
Format: DVD
Stars: Humphrey Bogart, Ingrid Bergman, Claude Rains, Peter Lorre

Title: Star Wars
Release Year: 1977
Format: Blu-Ray
Stars: Harrison Ford, Mark Hamill, Carrie Fisher, Alec Guinness, James Earl Jones
EOL

my $db     = File::Temp->new(DIR => '/tmp', SUFFIX => '.yaml');
my $dbtype = 'yaml'; # default db type is YAML
my $ctrl = Moviedb::Controller->new;
$ctrl->import_from_txt({ source_file => $db->filename, dbtype => $dbtype });
isa_ok( $ctrl, 'Moviedb::Controller', q/Test ISA option/);

# test import
my $fh = File::Temp->new(DIR => '/tmp', SUFFIX => '.txt');
my $fname = $fh->filename;
print $fh $txt_data;
qx(cat $fname);

$ctrl->import_from_txt({ source_file => $fname, dbtype => $dbtype });

# test search by id
my $m = $ctrl->get_movies_by_('id', 0);
ok($m->title eq 'Blazing Saddles', q/Movie's title/);
ok($m->release_year == 1974, q/Movie's release year/);
ok($m->format eq 'VHS', q/Movie's format/);

# test search by title or movie's name
my @m = @{ $ctrl->get_movies_by_('title', 'star') };
ok(scalar(@m) == 1, q/Found 1 movie by Title/);
ok($m[0]->title eq 'Star Wars', q/Movie's title is OK/);

# find movies by a star name
@m = @{ $ctrl->get_movies_by_('stars', 'ford') };
ok(scalar(@m) == 1, q/Found 1 movie by star name/);
ok(grep { $_ =~ /harrison/i } @m, q/There was Harrison Ford/);

# find movies by an year
@m = @{ $ctrl->get_movies_by_('release_year', 1977) };
ok(scalar(@m) == 1, q/Found 1 movie by Year/);
ok($m[0]->release_year == 1977, q/Movie's release year is OK/);

## clean up
foreach my $f (qw(moviedb.log moviedb.yaml)) {
    unlink $f;
}
