use strict;
use warnings;
use 5.010;

use Test::More "no_plan";
use Test::Trap;
use File::Temp qw/ tempfile /;
use Moviedb::Controller;

my $args = [
    'title=Star Wars', 'release_year=1977', 'format=Blu-Ray',
    'stars=Harrison Ford,Mark Hamill,Carrie Fisher,Alec Guinness,James Earl Jones'
];
qx{touch moviedb.yaml};
# add a new data - data set is OK
my $ctrl = Moviedb::Controller->new;
# switch logging to FATAL level
$ctrl->logger->adapter->{logger}->level('FATAL');
$ctrl->add_new_movie($args);

my @m = $ctrl->show_movies_by_('id', 1);
is(scalar @m, 1, q/Found one record/);
ok($m[0]->title eq 'Star Wars', q/Added a new record/);
diag explain @m;

# delete the record
my $id = $ctrl->delete_movie(1);
ok($id == 1, q/The record removed/);
# check it again
my $res = $ctrl->show_movies_by_('id', 1);
ok(! $res, q/The record not found/);

## clean up
foreach my $f (qw(moviedb.log moviedb.yaml)) {
    unlink $f;
}
