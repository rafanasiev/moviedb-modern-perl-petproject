use strict;
use warnings;
use 5.010;

use Test::Exception; 
use Test::FailWarnings;
use Test::More "no_plan";
use Test::Trap;

use Moviedb::Cinema;

# case #1 test OK class instantiation 
subtest 'Test for basic OO stuff' => sub {
    my $ok_args = {
        title        => 'The Movie',
        format       => 'DVD',
        release_year => 1977,
        stars        => 'John Doe, Ann Moss'
    };
    my $o = Moviedb::Cinema->new($ok_args);
    isa_ok( $o, 'Moviedb::Cinema');

    is($o->title, 'The Movie', 'Title is OK');
    is($o->format, 'DVD', 'DVD is OK');
    is($o->release_year, 1977, 'Release year is OK');
    is_deeply($o->stars, ['John Doe', 'Ann Moss'], 'Star list is OK');
};

# case #2 test for bad movie's formats
subtest 'Test for bad formats' => sub {
    my $not_ok_args = {
        title        => 'The Movie',
        format       => 'DVDRip',
        release_year => 1977,
        stars        => 'John Doe, Ann Moss'
    };
    trap { Moviedb::Cinema->new($not_ok_args) };
    like($trap->die, qr/does not supported/, q/Bad input argument - DVDRip format is wrong/);
};

# case #3 test bad star names
subtest 'Test for bad star names' => sub {
    my $not_ok_args = {
        title        => 'The Movie',
        format       => 'DVD',
        release_year => 1977,
        stars        => 'John Doe : Ann Moss'
    };
    my $o;
    throws_ok { $o = Moviedb::Cinema->new($not_ok_args) } 
        qr/Invalid input string/, q/Bad input argument - star's string is wrong/;
    diag explain $o;
    $not_ok_args->{stars} = 'John Doe101 Ann Moss';
    throws_ok { Moviedb::Cinema->new($not_ok_args) }
        qr/Invalid input string/, q/Bad input argument - star's name is bad/;
};


# case #4 test NOT OK class instantiation - bad value for release_year
subtest 'Test for bad release_year' => sub {
    my $not_ok_args = {
        title        => 'The Movie',
        format       => 'DVD',
        release_year => 'FF11',
        stars        => 'John Doe, Ann Moss'
    };
    trap { Moviedb::Cinema->new($not_ok_args) };
    like($trap->die, qr/Value "FF11" did not pass type constraint/, q/Bad input argument - release year is invalid/);

};

# case #5 test NOT OK class instantiation
subtest 'Test for bad titles' => sub {
    my $not_ok_args = {
        title        => '',
        format       => 'DVD',
        release_year => 1976,
        stars        => 'John Doe,Ann Moss'
    };
    throws_ok { Moviedb::Cinema->new($not_ok_args) }
        qr/cannot be empty/, q/Bad input argument - title is empty/;

    $not_ok_args->{title} = undef;
    trap { Moviedb::Cinema->new($not_ok_args) };
    like($trap->die, qr/cannot be empty/, q/Bad input argument - title is empty/);
};

## clean up
foreach my $f (qw(moviedb.log moviedb.yaml)) {
    unlink $f;
}

