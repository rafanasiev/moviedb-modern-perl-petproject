use strict;
use warnings;
use 5.010;

use Test::More "no_plan";
use Test::Trap;
use Data::Dumper;

BEGIN {
    use_ok( 'Moviedb::Cinema' ) || print "Bail out!\n";
}

diag( "Testing Moviedb::Cinema $Moviedb::Cinema::VERSION, Perl $], $^X" );

