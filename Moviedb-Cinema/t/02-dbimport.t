use strict;
use warnings;
use 5.010;

use Test::More "no_plan";
use Test::Trap;
use File::Temp qw/ tempfile /;
use Moviedb::Controller;

# txt input data mimic
my $txt_data =<<EOL;
Title: Blazing Saddles
Release Year: 1974
Format: VHS
Stars: Mel Brooks, Clevon Little, Harvey Korman, Gene Wilder, Slim Pickens, Madeline Kahn

Title: Casablanca
Release Year: 1942
Format: DVD
Stars: Humphrey Bogart, Ingrid Bergman, Claude Rains, Peter Lorre

Title: Star Wars
Release Year: 1977
Format: Blu-Ray
Stars: Harrison Ford, Mark Hamill, Carrie Fisher, Alec Guinness, James Earl Jones
EOL

my $dbtype = 'yaml'; # default db type is YAML
# controller
my $ctrl = Moviedb::Controller->new;
isa_ok($ctrl, 'Moviedb::Controller');

# test import
my $fh = File::Temp->new(DIR => '/tmp', SUFFIX => '.txt');
my $fname = $fh->filename;
print $fh $txt_data;
qx(cat $fname);

my $result = $ctrl->import_from_txt({source_file => $fname, dbtype => $dbtype});
diag explain $result;
cmp_ok($result, '==', 3, q/Import from TXT file/);

# test add a new record

## clean up
foreach my $f (qw(moviedb.log moviedb.yaml)) {
    unlink $f;
}
