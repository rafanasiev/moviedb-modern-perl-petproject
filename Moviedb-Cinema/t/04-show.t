use strict;
use warnings;
use 5.010;

use Test::More "no_plan";
use Test::Trap;
use File::Temp qw/ tempfile /;
use Moviedb::Controller;

# txt input data mimic
my $txt_data =<<EOL;
Title: Star Wars
Release Year: 1977
Format: Blu-Ray
Stars: Harrison Ford, Mark Hamill, Carrie Fisher, Alec Guinness, James Earl Jones
EOL
# create a temp file
my $fh = File::Temp->new(DIR => '/tmp', SUFFIX => '.txt');
my $fname = $fh->filename;
print $fh $txt_data;
qx(cat $fname);
my $dbtype = 'yaml'; # default db type is YAML

# import data
my $ctrl = Moviedb::Controller->new;
# switch logging to FATAL level
$ctrl->logger->adapter->{logger}->level('FATAL');
$ctrl->import_from_txt({ source_file => $fname, dbtype => $dbtype });

# check release_year option
my @m = $ctrl->show_movies_by_('year');
my $res = $m[0]->release_year;
ok($res == 1977, qq/Movie's release year is OK - $res/);
# check by title
@m = $ctrl->show_movies_by_('title');
$res = $m[0]->title;
ok($res eq 'Star Wars', qq/Movie's title is OK - $res/);
# check by format 
@m = $ctrl->show_movies_by_('format');
$res = $m[0]->format;
ok($res eq 'Blu-Ray', qq/Movie's format is OK - $res/);

## clean up
foreach my $f (qw(moviedb.log moviedb.yaml)) {
    unlink $f;
}
